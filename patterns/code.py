# Analog to digital converter example.
# Will loop forever printing ADC channel 1 raw and mV values every second.
# NOTE the ADC can only read voltages in the range of ~900mV to 1800mV!

import time
import board
import busio
import adafruit_lis3dh
import digitalio
import microcontroller
import neopixel

#CONSTANT file Format
version = 1
delay = 0.001



# define custom colors
RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)
WHITE = (255, 255, 255)
OFF = (0, 0, 0)

pixels = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=0.2, auto_write=False)

button_stop = digitalio.DigitalInOut(board.D4)
button_stop.direction = digitalio.Direction.INPUT
button_stop.pull = digitalio.Pull.DOWN

button_start = digitalio.DigitalInOut(board.D5)
button_start.direction = digitalio.Direction.INPUT
button_start.pull = digitalio.Pull.DOWN

accel_range = [-8,8] # magic range relative to accel sensibility
range_values = accel_range[1] - accel_range[0] # 16

def adjust_brightness(value):
    if value > 8:
        return 1
    if value < -8:
        return 0.001
    bright = (value-accel_range[0])/range_values
    return bright or 0.001
    

def theater_chase(color1, color2):
    index = 0
    size = len(pixels)
    while True
        for i in range(size):
            if (i + index) % 3 == 0:
                pixels[i] = color1
            else:
                pixels[i] = color2
        pixels.show()
        yield
                
def colorWipe(color1):
    size = len(pixels)
    index = 0
    while True:
        pixels.fill((0,0,0))
        pixels[index % size] = color1
        pixels.show()
        index += 1
        yield


def fade(color1, color2, size=10):
    (red1, green1, blue1) = color1
    pixels.fill(color1)
    pixels.show()
    (red2, green2, blue2) = color2
    for index in range(size):
        red = (red1 * (size - index) + red2*index)/size
        green = (green1 * (size - index) + green2*index)/size
        blue = (blue1 * (size - index) + blue2*index)/size
        pixels.fill((int(red), int(green), int(blue)))
        pixels.show()
        yield
    
    
def scanner(color1, size = 10):
index = 0
while True
    for i in range(len(pixels)):
        if i == (index % len(pixels)):
            pixels[i] = color1
            else:
                (r, g, b) = pixels[i]
                r = r >> 1
                g = g >> 1
                b = b >> 1
                pixels[i] = (r,g,b)
    pixels.show()
    index += 1
    yield

def save_data(file_open):
    pixels.fill(PURPLE)
    pixels.show();
    while True:
            x,y,z = (
                value / adafruit_lis3dh.STANDARD_GRAVITY for value in lis3dh.acceleration
            )
            file_open.write('{0:f} ; {1:f}; {2:f}\n'.format(x,y,z))
            #print('%0.3f ; %0.3f ; %0.3f' % (x,y,z))
            #file_open.flush()
            time.sleep(delay)
            if button_stop.value :
                pixels.fill(RED)
                pixels.show()
                file_open.flush()
                increase_color(GREEN,0.1)
                break;


def increase_color(color, wait, steps=32):
    r,g,b = color
    for i in range(steps):
        pixels.fill((int(r*i/steps), int(g*i/steps), int(b*i/steps)))
        time.sleep(wait)
        pixels.show()

i2c = busio.I2C(board.ACCELEROMETER_SCL, board.ACCELEROMETER_SDA)
lis3dh = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)
lis3dh.range = adafruit_lis3dh.RANGE_8_G

led = digitalio.DigitalInOut(board.D13)
led.switch_to_output()

try:
    name = 1
    while True:
        if button_start.value:
            increase_color(BLUE, 0.1)
            with open("/accel-"+str(name)+".txt", "a") as fp:
                fp.write('version:{},delay:{}\n'.format(version, delay))
                fp.flush()
                save_data(fp)
            name += 1
        time.sleep(0.5)
except OSError as e:
    wait = 0.5
    if e.args[0] == 28:
        wait = 0.25
    while True:
        pixels.fill(RED)
        pixels.show()
        time.sleep(wait)
        pixels.fill(OFF)
        pixels.show()
        time.sleep(wait)
        
    

## Set range of accelerometer (can be RANGE_2_G, RANGE_4_G, RANGE_8_G or RANGE_16_G).
#a,b,c = 0, 0, 0
## Loop forever printing accelerometer values
#while True:
    ## Read accelerometer values (in m / s ^ 2).  Returns a 3-tuple of x, y,
    ## z axis values.  Divide them by 9.806 to convert to Gs.
        #a = max(x,a)
        #b = max(b,y)
        #c = max(c,z)
    ## Small delay to keep things responsive but give time for interrupt processing.
    #time.time.sleep(0.1)
