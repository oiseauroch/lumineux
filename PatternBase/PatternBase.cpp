#include"PatternBase.h"


/**
 * @brief fill strip with the specified color
 * 
 *  not showing colors immediatly in the strip , need \ref show() to reflet modifications
 * 
 * @param color color to fill
 */
void MyNeopixel::fill(color_t color)
{
    for (uint8_t i = 0 ; i < numPixels(); ++i)
    {
        setPixelColor( i, color);
    }
}

/**
 * @brief add functionnality to Adafruit_NeoPixel class.
 * 
 * @param t_strip Adafruit_NeoPixel strip to use
 * @param t_nbrLed number of led of the strip
 * @param first_led in case of substrip, led considered as the first one
 */
MyNeopixel::MyNeopixel(Adafruit_NeoPixel& t_strip, uint8_t first_led, uint8_t t_nbrLed )
:strip(t_strip), add_index(first_led), nbrLed(t_nbrLed)
{}

/**
 * @brief rerturn number of led in the strip
 * 
 * @return number of led in the strip
 */
uint8_t MyNeopixel::numPixels()
{
    return this->nbrLed;
}

/**
 * @brief set pixel with index i with the color color
 * 
 *  not showing colors immediatly in the strip , need \ref show() to reflet modifications
 * 
 * @param index index of the color
 * @param color color to set
 */
void MyNeopixel::setPixelColor(uint8_t index, color_t color)
{
    this->strip.setPixelColor(index + add_index, color.r, color.g, color.b);
}

/**
 * @brief display modification in the strip
 * 
 */
void MyNeopixel::show()
{
    this->strip.show();
}



String PatternBase::name = "PatternBase";

/**
 * @brief compute and display next iteration of the pattern
 * 
 */
void PatternBase::next()
{
    ++index;
    this->setPattern();
    this->strip.show();

}

/**
 * @brief compute and display previous iteration of the pattern
 * 
 */
void PatternBase::prev()
{
    --index;
    this->setPattern();
    this->strip.show();

}

/**
 * @brief set new color 
 * 
 * @param t_color new color to set
 */
void PatternBase::setColor ( color_t t_color)
{
    color = t_color;
}

/**
 * @brief display color on led one after an other
 * 
 * @param t_color color to display
 * @param t_strip strip to display values
 * @param t_size size of the strip
 */
ColorWipePattern::ColorWipePattern(color_t t_color, MyNeopixel& t_strip, uint8_t t_index)
:PatternBase(t_color, t_strip, t_index)
{
}


/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void ColorWipePattern::setPattern()
{
    this->strip.fill(color_t(0,0,0));
    this->strip.setPixelColor(index % size, color);
}

/**
 * @brief make transition from one color to an other
 * 
 * @param t_begin initial color
 * @param t_end final color
 * @param t_strip led strip.
 * @param t_steps number of steps
 */
FadePattern::FadePattern ( color_t t_begin, color_t t_end, MyNeopixel& t_strip, uint16_t t_nbrIter, bool twoSides)
: PatternBase(t_begin, t_strip, 0), begin(this->color), end(t_end), m_twosides(twoSides)
{
    this->nbrIter = t_nbrIter;
    
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void FadePattern::setPattern()
{
    
    uint16_t red, green, blue;
    

    if (m_twosides)
    {
        if(index == 255)
        {
            index = nbrIter*2;
        } else if (index > nbrIter*2)
        {
            index = 0;
        }
    } else {
        if (index == 255)
        {
            index = nbrIter;
        } else if (index > nbrIter )
        {
            index = 0;
        }
        
    }
        if(m_twosides and index > nbrIter)
        {
                    red = (begin.r * (index - nbrIter) + end.r*(2*nbrIter - index))/nbrIter;
                    green = (begin.g * (index - nbrIter) + end.g*(2*nbrIter - index))/nbrIter;
                    blue = (begin.b * (index - nbrIter) + end.b*(2*nbrIter - index))/nbrIter;
        } else {
        red = (begin.r * (nbrIter - index) + end.r*index)/nbrIter;
        green = (begin.g* (nbrIter - index) + end.g*index)/nbrIter;
        blue = (begin.b* (nbrIter - index) + end.b*index)/nbrIter;    
        }
        this->strip.fill(color_t(red, green, blue));
}


/**
 * @brief set initial and final color 
 * 
 * @param t_begin initial color.
 * @param t_end final color
 */
void FadePattern::setColor(color_t t_begin, color_t t_end)
{
    this->begin = t_begin;
    this->end = t_end;
}

/**
 * @brief pattern with one full brightness led and following leds with brightness decreasing
 * 
 * @param t_color color of the led
 * @param t_strip strip
 */
ScannerPattern::ScannerPattern(color_t t_color, MyNeopixel& t_strip, uint8_t t_index)
:PatternBase(t_color, t_strip, t_index)
{
}


/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void ScannerPattern::setPattern()
{
    uint8_t i = 0;
    uint8_t led = this-> index % this->strip.numPixels();
    for (i = 0; i <= led ; ++i)
    {
        uint8_t shift = led - i ;
        this->strip.setPixelColor(i, color >> shift);
    }
    for ( ; i < this->strip.numPixels() ; ++i)
    {
        uint8_t shift = led + this->strip.numPixels() - i;
        this->strip.setPixelColor(i, color >> shift);

    }
}


/**
 * @brief display rainbow moving on the strip
 * 
 * @param t_strip strip
 * @param t_index initial index (default 0)
 */
RainbowPattern::RainbowPattern(MyNeopixel& t_strip, uint8_t t_index)
:PatternBase(color_t(), t_strip, t_index)
{
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void RainbowPattern::setPattern()
{
    for (uint8_t i ; i < this->size; ++i)
    {
        uint8_t pixIndex = (i * 256 / this->size) + index;
        this->strip.setPixelColor(i, wheel(pixIndex & 255));
    }
}

/**
 * @brief Input a value 0 to 255 to get a color value.
 *  The colors are a transition red -> green -> blue -> back to red
 * @param pos position of the wheel
 * @return compouted color
 */
color_t RainbowPattern::wheel(uint8_t pos)
{
    pos = 255 - pos;
    if (pos < 85) {
        return color_t(255 - (pos * 3), 0, pos * 3);
    } else if (pos < 170) {
        pos -= 85;
        return color_t(0, pos * 3, 255 - (pos * 3));
    } else {
        pos -= 170;
        return color_t(pos * 3, 255 - (pos * 3), 0);
    }
}

/**
 * @brief fill strip with a color, then apply random modification to each pixel. 
 * 
 * @param t_color initial color
 * @param t_strip strip
 * @param stable amplitude of the random modifcation from 1 (max amplitude) to 255(min amplitude)
 * @param fill fill with initial color at the beginning or not
 */
FillRandomPattern::FillRandomPattern(color_t t_color, MyNeopixel& t_strip, uint8_t stable, bool fill):
PatternBase(t_color, t_strip, 0), m_stable(stable)
{
    if (fill){
        this->strip.fill(t_color);
    }
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void FillRandomPattern::setPattern()
{
    for (uint8_t i = 0 ; i < this->size; ++i)
    {
        auto color = this->strip.getPixelColor(i);
        uint8_t value = random(0,255)/m_stable;
        auto add_value = value + (value << 8) +  (value << 16) ;
        color_t new_color;
        if(random(0,1))
        {
            new_color = color + add_value;
        } else 
        {
            new_color = color - add_value;            
        }
          this->strip.setPixelColor(i, new_color );   
    }
}

/**
 * @brief fill strip with a color then apply same random modification to all the strip
 * 
 * @param t_color iniital color
 * @param t_strip strip
 * @param stable amplitude of the random modifcation from 1 (max amplitude) to 255(min amplitude)
 * @param fill fill with initial color at the beginning or not
 */
FillUnstablePattern::FillUnstablePattern::FillUnstablePattern(color_t t_color, MyNeopixel& t_strip, uint8_t stable, bool fill):
PatternBase(t_color, t_strip, 0), m_fill(fill), m_stable(stable)
{
    if (fill){
        this->strip.fill(color);
    }
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void FillUnstablePattern::setPattern()
{
    auto local_color = this->strip.getPixelColor(0);
    uint8_t value = random(0,255)/m_stable;
    auto add_value = value + (value << 8) +  (value << 16) ;
    this->strip.fill(color_t(local_color +add_value));
}

/**
 * @brief set pixel to init
 * 
 */
void FillUnstablePattern::reinit()
{
        this->strip.fill(color);    
        this->strip.show();
}


/**
 * @brief fade from \ref t_color1 to \ref t_color2 with random modification for each pixel
 * 
 * @param t_color1 iniital color
 * @param t_color2 final color
 * @param t_strip strip
 * @param stable amplitude of the random modifcation from 1 (max amplitude) to 255(min amplitude)
 * @param nbr_iter number of iteration between initial and final color
 * @param twoSide do initial -> final -> initial.
 */
FadeRandomPattern::FadeRandomPattern(color_t t_color1, color_t t_color2, MyNeopixel& t_strip, uint8_t stable, uint16_t nbr_iter, bool twoSide):
PatternBase(color_t(), t_strip) ,FillRandomPattern(color_t(), t_strip, stable, false), FadePattern(t_color1, t_color2, t_strip, nbr_iter, twoSide)
{
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void FadeRandomPattern::setPattern()
{
    FadePattern::setPattern();
    FillRandomPattern::setPattern();
}


FadeUnstablePattern::FadeUnstablePattern(color_t t_color1, color_t t_color2, MyNeopixel& t_strip, uint8_t stable, uint16_t nbr_iter, bool twoSide):
PatternBase(color_t(), t_strip) ,FillUnstablePattern(color_t(), t_strip, stable, false), FadePattern(t_color1, t_color2, t_strip, nbr_iter, twoSide)
{
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void FadeUnstablePattern::setPattern()
{
    FadePattern::setPattern();
    FillUnstablePattern::setPattern();
}


/**
 * @brief [WARNING] not customizable pattern. Fade blue->white->yellow->orange->red
 * 
 * @param t_strip strip
 * @param nbr_iter total number of  iterations
 */
MultiFadePattern::MultiFadePattern(MyNeopixel& t_strip, uint16_t nbr_iter):
PatternBase(color_t(), t_strip), FadePattern(color_t(), color_t(), t_strip, nbr_iter),index_color(0)
{
    colors[0] = color_t(0x80, 0xBC, 0xFF);
    colors[1] = color_t(0xff, 0xff, 0xff);
    colors[2] = color_t(0xFF, 0xFF, 0x00);
    colors[3] = color_t(0xFF, 0x7C, 0x00);
    colors[4] = color_t(0xF5, 0x10, 0x00);
    
    this->nbrIter = nbr_iter/4;
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void MultiFadePattern::setPattern()
{
    if(index == 255)
    {
        index = nbrIter;
        index_color --;
    } else if (index > nbrIter)
    {
        index = 0;
        index_color ++;
    }
    this->begin = colors[(index_color)% 5];
    this->end= colors[(index_color+1)% 5];
    FadePattern::setPattern();
}

/**
 * @brief compute  iteration pattern 'index'
 * 
 */
void SimpleColorPattern::setPattern()
{
    this->strip.fill(color);
}


/**
 * @brief fill strip with a color
 * 
 * @param color color to fill
 * @param t_strip strip
 */
SimpleColorPattern::SimpleColorPattern(color_t color, MyNeopixel& t_strip):
PatternBase(color, t_strip)
{
}


/**
 * @brief … display a ring  on the strip
 * 
 * @param color p_color:...
 * @param t_strip p_t_strip:...
 * @param t_index p_t_index:...
 */
RingPattern::RingPattern ( color_t t_color, MyNeopixel& t_strip,uint8_t ring_size, uint8_t t_index ):
PatternBase(t_color, t_strip, t_index),m_ring_size(ring_size)
{
    nbrIter = t_strip.numPixels()/ring_size;
}

void RingPattern::setPattern()
{
    if (index == 255) 
    {
        index = nbrIter -1;
    }
    else if (index >= nbrIter)
    {
        index = 0;
    }
    this->strip.fill(color_t(0,0,0));
    uint8_t first_led = index*m_ring_size ;
    for (int j = first_led; j < first_led + m_ring_size;  ++j)
    {
    this->strip.setPixelColor(j, color);
    }

}

