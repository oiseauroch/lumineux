#pragma once

#include <Adafruit_NeoPixel.h>

/**
 * @brief color class contain a red, green and red component
 * can be constructed with the following options : 
 *  -  color_t(uint32_t color_00RRGGBB)
 *  -  color_t(int r, int g, int b) with value from 0 to 255
 * 
 */
struct color_t{
   uint8_t r;
   uint8_t g;
   uint8_t b;
   color_t(uint32_t color):
   r((color & 0xFF0000) >> 16), g((color & 0xff00) >> 8), b(color & 0xff){};
   color_t():
   r(0),g(0), b(0) {};
   color_t(int red, int green, int blue):
   r(red),g(green), b(blue) {};
   color_t  operator>>(uint8_t shift)
   {
       return color_t(r >>shift, g >> shift, b >> shift);
};
   color_t  operator<<(uint8_t shift)
   {
       return color_t(r << shift, g << shift, b << shift);
};
   
};




/**
 * @brief Manage neopixel strip. 
 * Enable substrip support and fill method
 * 
 */
class MyNeopixel
{
public :
   MyNeopixel(Adafruit_NeoPixel& t_strip, uint8_t first_led = 0, uint8_t t_nbrLed = 10);
   void fill(color_t color);
   void setPixelColor(uint8_t index, color_t color);
   void show();
   uint32_t getPixelColor(uint16_t n){return this->strip.getPixelColor(n);};
   uint8_t numPixels();
private :
  Adafruit_NeoPixel& strip;     
  uint8_t add_index;
  uint8_t nbrLed;
};

/**
 * @brief virtual base class for creating pattern
 * 
 */
class PatternBase{

public :
    PatternBase(color_t  t_color, MyNeopixel& t_strip,  uint t_index = 0):
    strip(t_strip), color(t_color), index(t_index), size(t_strip.numPixels()), nbrIter(t_strip.numPixels()) {};
    void next();
    void prev();
    void reinit() { index = 0;};
    void setColor(color_t t_color);
    uint8_t getnbrIter() { return nbrIter;};
    uint8_t getCurrentIter() {return index % nbrIter; };
    static String name;
protected:
    virtual void setPattern() = 0;
    MyNeopixel& strip; 
    color_t color; ///< default color
    uint8_t index; ///< current index operation
    uint8_t size; ///< number of pixels
    uint16_t nbrIter; ///< nbr of iterations

};

/**
 * @brief context object for a pattern, contain a pointer to a  pattern and the periodicity of the pattern
 * 
 */
struct context {
  PatternBase* pattern;
  uint16_t time_ms = 100;
  context(PatternBase* p, uint16_t t = 100):pattern(p), time_ms(t) {};
};

/**
 * @brief color pixels one after an other
 * 
 */
class ColorWipePattern: virtual public PatternBase{
public:
    ColorWipePattern(color_t t_color, MyNeopixel& t_strip, uint8_t t_index = 0);
    
protected:
    void setPattern() override;
};

/**
 * @brief fade strip from an initial color to a final one with configurable number of steps
 * 
 */
class FadePattern: virtual public PatternBase{
public:
    FadePattern(color_t t_begin, color_t t_end, MyNeopixel& t_strip, uint16_t t_nbrIter, bool twoSides = false);
    void setColor(color_t t_begin, color_t t_end);
protected:
    void setPattern() override;
    color_t& begin;
    color_t end;
    bool m_twosides;
};


/**
 * @brief turn on one pixel then move to the next and fade to black previous ones
 * 
 */
class ScannerPattern: virtual public PatternBase {
public:
    ScannerPattern(color_t t_color, MyNeopixel& t_strip, uint8_t t_index = 0);
protected:
    void setPattern() override;
};

/**
 * @brief display a rainbow moving pattern
 * 
 */
class RainbowPattern: virtual public PatternBase {
public:
    RainbowPattern(MyNeopixel& t_strip, uint8_t t_index = 0);
protected:
    void setPattern() override;
private:
    color_t wheel(uint8_t pos);
};

/**
 * @brief NOT YET IMPLEMENTED
 * 
 */
class WipeCirclePattern: virtual public PatternBase {
public:
    WipeCirclePattern(color_t t_color, MyNeopixel& t_strip,  uint8_t t_index = 0);
protected:
    void setPattern() override;
};

/**
 * @brief fill color on the strip then apply random variation to each pixel
 * 
 */
class FillRandomPattern: virtual public PatternBase {
public:
    FillRandomPattern(color_t t_color, MyNeopixel& t_strip, uint8_t stable = 1, bool fill = true);
protected:
    void setPattern() override;
private:
    uint8_t m_stable;
};

/**
 * @brief fill color on the strip then apply random variation to all the strip
 * 
 */
class FillUnstablePattern: virtual public PatternBase {
public:
    FillUnstablePattern(color_t t_color, MyNeopixel& t_strip, uint8_t stable = 1, bool fill = true);
protected:
    void setPattern() override;
    void reinit();
private:
    bool m_fill;
    uint8_t m_stable;
};

/**
 * @brief combine fade and fille unstable 
 * 
 */
class FadeUnstablePattern: public FillUnstablePattern, public FadePattern {
public:
    FadeUnstablePattern(color_t t_color1, color_t t_color2, MyNeopixel& t_strip, uint8_t stable = 1, uint16_t nbr_iter = 256  , bool twoSide = false);
private:
    uint8_t m_stable;
    void setPattern() override;
};


/**
 * @brief combine fade and fill random
 * 
 */
class FadeRandomPattern: public FillRandomPattern, public FadePattern {
public:
    FadeRandomPattern(color_t t_color1, color_t t_color2, MyNeopixel& t_strip, uint8_t stable = 1, uint16_t nbr_iter = 256  , bool twoSide = false);
private:
    uint8_t m_stable;
    void setPattern() override;
};

/**
 * @brief not configurable pattern to display 5  fade colors one after an other
 * 
 */
class MultiFadePattern: public FadePattern {
public:
    MultiFadePattern( MyNeopixel& t_strip, uint16_t nbr_iter = 256);
private:
   color_t  colors[5];
   uint8_t index_color;
    void setPattern() override; 
};

/**
 * @brief display a color on the strip
 * 
 */
class SimpleColorPattern: public PatternBase {
public:
    SimpleColorPattern(color_t color, MyNeopixel& t_strip);
    void reinit() {this->strip.fill(color); this->strip.show();};
private:
   void setPattern() override;
};

/**
 * @brief display a ring  on the strip
 * 
 */
class RingPattern: public PatternBase {
public:
    RingPattern(color_t t_color, MyNeopixel& t_strip, uint8_t ring_size = 5, uint8_t t_index = 0);
private:
   void setPattern() override;
   uint8_t m_ring_size;
};


