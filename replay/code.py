# Analog to digital converter example.
# Will loop forever printing ADC channel 1 raw and mV values every second.
# NOTE the ADC can only read voltages in the range of ~900mV to 1800mV!

import time
import board
import busio
import adafruit_lis3dh
import digitalio
import microcontroller
import neopixel

# define custom colors
RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)
WHITE = (255, 255, 255)
OFF = (0, 0, 0)

# delay constant
delay = 0

pixels = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=0.2, auto_write=False)

button_start = digitalio.DigitalInOut(board.D5)
button_start.direction = digitalio.Direction.INPUT
button_start.pull = digitalio.Pull.DOWN

def increase_color(color, wait = 0.05, steps=32):
    r,g,b = color
    for i in range(steps):
        pixels.fill((int(r*i/steps), int(g*i/steps), int(b*i/steps)))
        time.sleep(wait)
        pixels.show()

def parse_metadata(metadata):
    global delay
    for data in metadata:
        (name, value)  = data.split(':')
        print(data)
        if name == "version":
            if int(value) !=1 and int(value) !=2 :
                raise ValueError("version not supported")
        elif name == "delay":
            delay = float(value)
        else : 
            print(name)
            return False
    return True

def scanner(color1):
    index = 0
    while True:
        for i in range(len(pixels)):
            if i == (index % len(pixels)):
                pixels[i] = color1
            else:
                (r, g, b) = pixels[i]
                r = r >> 1
                g = g >> 1
                b = b >> 1
                pixels[i] = (r,g,b)
        pixels.show()
        index += 1
        yield
        

def adjust_brightness(value): # function with magic number for sensibility of 8G
    if value > 0:
        return 1
    if value < -8:
        return 0.001
    bright = (value+8)/8
    print(bright)
    return bright or 0.001


def colorWipe(color1):
    size = len(pixels)
    index = 0
    while True:
        pixels.fill((0,0,0))
        pixels[index % size] = color1
        pixels.show()
        index += 1
        yield

scan = colorWipe((255,0,0))

def process_file(x,y,z):
    scan.__next__()
    #do operations here

try:
    while True:
        pixels.fill((125,75,0))
        pixels.show()
        while not button_start.value:
            time.sleep(0.1)
        increase_color(BLUE)
        with open("/replay.txt", "r") as fp:
            metadata = fp.readline().split(',')
            if not parse_metadata(metadata):
                raise RuntimeError("Rincewind  came et ran away")
            index = 0
            print(delay)
            for line in fp:
                (x,y,z) = line.split(';')
                x = float(x)
                y = float(y)
                z = float(z)
                pixels.brightness = adjust_brightness(x+y+z)
                pixels.show()
                if (index % 5) == 0:
                    process_file(x,y,z)
                index+=1
                time.sleep(delay*5)
except OSError as e:
    print(e)
    pixels.fill(RED)
except ValueError as e:
    print(e)
    pixels.fill(CYAN)
except RuntimeError as e:
    print(e)
    pixels.fill((100,100,100))
pixels.show()
while True:
    pass
    

## Set range of accelerometer (can be RANGE_2_G, RANGE_4_G, RANGE_8_G or RANGE_16_G).
#a,b,c = 0, 0, 0
## Loop forever printing accelerometer values
#while True:
    ## Read accelerometer values (in m / s ^ 2).  Returns a 3-tuple of x, y,
    ## z axis values.  Divide them by 9.806 to convert to Gs.
        #a = max(x,a)
        #b = max(b,y)
        #c = max(c,z)
    ## Small delay to keep things responsive but give time for interrupt processing.
    #time.time.sleep(0.1)
