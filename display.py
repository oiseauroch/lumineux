#! /usr/bin/python
import matplotlib.pyplot as plt
import numpy as np
import numpy.fft as fft
import sys
import argparse
import matplotlib.gridspec as gridspec
import math

parser = argparse.ArgumentParser()
parser.add_argument("--partial", action="store_true", help="plot sum(x,y), sum(y,z), sum(x,z)")
parser.add_argument("--total", action="store_true", help="plot sum(x,y,z)")
parser.add_argument("--normalize", action="store_true", help="normalize around 0")
parser.add_argument("--average", type=int, help="perform a moving average with 2n+1 terms", default=0)
parser.add_argument("--end", type=float, default=0, help="end at  t time")
parser.add_argument("--begin", type=float, default=0, help="begin at t time")
parser.add_argument("--fft", action="store_true", help=("display fast fourrier transform"))
parser.add_argument("files", nargs="+")
args = parser.parse_args()

data = []
time_axis = []

def parse_metadata(meta):
    metadata = {}
    meta = meta.split(',')
    for data in meta:
        (name, value)  = data.split(':')
        metadata[name] = float(value)
    if metadata["version"] > 2 : 
        raise ValueError("version not supported")
    return metadata

def parse_file(filename):
    with open(filename) as fd:
        metadata = fd.readline()
        meta = parse_metadata(metadata)
        data =  np.genfromtxt(fd, delimiter=";", names=['x','y','z']) 
        return (data,meta)

for file in args.files:
    (d,m) = parse_file(file)
    data.append(d)
    time_axis.append(m['delay'])
    
#data = [np.genfromtxt(skipper(file), delimiter=";", names=['x','y','z']) for file in args.files]

def average(data, n):
    average = []
    for i, value in enumerate(data):
        for j in range(1, n):
            if (i < j):
                value += data[0]
            else:
                value += data[i-j]
            if i+j >= len(data):
                value += data[len(data)-1]
            else:
                value+= data[i+j]
        average.append(value/(2*n+1))
    return average


    
def compute_fft(data, period, freqMax=6):
    fourrier = fft.fft(data)/len(data)
    timePeriod = len(data)*period
    fourrier = fourrier[0:int(freqMax*timePeriod)]
    frequencies = np.arange(0, len(fourrier))/timePeriod
    return (frequencies, abs(fourrier))

fig = plt.figure(constrained_layout=True)
lines = (args.total + args.partial  + 1)
if args.fft:
    lines *= 2
gs = fig.add_gridspec(lines, 3)
plt.suptitle(" data["+ str(args.begin)+","+str(len(data[0]['x'])-args.end)+"]"+" normalize : "+str(args.normalize)+" average : "+str(args.average))


x = fig.add_subplot(gs[0,0])
y = fig.add_subplot(gs[0,1])
z = fig.add_subplot(gs[0,2])
x.set_title("x axis")
y.set_title("y axis")
z.set_title("z axis")
if args.fft:
    fft_x = fig.add_subplot(gs[1+args.partial+args.total,0])
    fft_y = fig.add_subplot(gs[1+args.partial+args.total,1])
    fft_z = fig.add_subplot(gs[1+args.partial+args.total,2])
    fft_x.set_title("fft x axis")
    fft_y.set_title("fft y axis")
    fft_z.set_title("fft z axis")
    
if args.total:
    total = fig.add_subplot(gs[1, :])
    total.set_title("xyz sum")
    if args.fft:
        fft_total = fig.add_subplot(gs[2+args.partial+args.total,:])
        fft_total.set_title("fft total sum")

if args.partial:    
    partial_x = fig.add_subplot(gs[1+args.total, 0])
    partial_x.set_title("xy sum")
    partial_y = fig.add_subplot(gs[1+args.total, 1])
    partial_y.set_title("yz sum")
    partial_z = fig.add_subplot(gs[1+args.total, 2])
    partial_z.set_title("xz sum")
    if args.fft:
        fft_partial_xy = fig.add_subplot(gs[2+args.partial+2*args.total,0])
        fft_partial_xy.set_title("fft partial xy sum")
        fft_partial_yz = fig.add_subplot(gs[2+args.partial+2*args.total,1])
        fft_partial_yz.set_title("fft partial yz sum")
        fft_partial_xz = fig.add_subplot(gs[2+args.partial+2*args.total,2])
        fft_partial_xz.set_title("fft partial xz sum")

legends = []
for index,table in enumerate(data):
    end = len(table['x'])
    if(args.end != 0):
        end = int(args.end/time_axis[index])
    begin  = int(args.begin/time_axis[index])
    print(begin, end)

    abs_axis = np.linspace(0,len(table['x'])*time_axis[index], len(table['x']))
    data_x = table['x'][begin: end]
    data_y = table['y'][begin: end]
    data_z = table['z'][begin: end]
    abs_axis = abs_axis[begin:end]
    
    if args.normalize:
        norm_x = sum(data_x)/len(data_x)
        norm_y = sum(data_y)/len(data_y)
        norm_z = sum(data_z)/len(data_z)
        data_x = data_x-norm_x
        data_y = data_y-norm_y
        data_z = data_z-norm_z
        
    if args.average > 0:
        data_x = average(data_x, args.average)
        data_y = average(data_y, args.average)
        data_z = average(data_z, args.average)
                
    legends.append(x.plot(abs_axis, [x for x in data_x], label=args.files[index]))
    y.plot(abs_axis, [y for y in data_y])
    z.plot(abs_axis, [z for z in data_z])
    
    if args.fft:
        (freq, fft_data) = compute_fft(data_x, time_axis[index])
        fft_x.plot(freq, fft_data)
        (freq, fft_data) = compute_fft(data_y, time_axis[index])
        fft_y.plot(freq, fft_data)
        (freq, fft_data) = compute_fft(data_z, time_axis[index])
        fft_z.plot(freq, fft_data)

    
    if args.total:
        data_xyz = [x+y+z for (x,y,z) in zip(data_x, data_y, data_z)]
        total.plot(abs_axis, data_xyz)
        if args.fft:
            (freq, fft_data) = compute_fft(data_xyz, time_axis[index])
            fft_total.plot(freq, fft_data)
            

    if args.partial:
        data_xy = [x+y for (x,y) in zip(data_x, data_y)]
        data_yz = [y-z for (y,z) in zip(data_y, data_z)]
        data_xz = [x-z for (x,z) in zip(data_x, data_z)]
        partial_x.plot(abs_axis, data_xy)
        partial_y.plot(abs_axis, data_yz)
        partial_z.plot(abs_axis, data_xz)
        if args.fft:
            (freq, fft_data) = compute_fft(data_xy, time_axis[index])
            fft_partial_xy.plot(freq, fft_data)
            (freq, fft_data) = compute_fft(data_yz, time_axis[index])
            fft_partial_yz.plot(freq, fft_data)
            (freq, fft_data) = compute_fft(data_xz, time_axis[index])
            fft_partial_xz.plot(freq, fft_data)
        
handles, labels = x.get_legend_handles_labels()
fig.legend(handles, labels)
plt.show()

