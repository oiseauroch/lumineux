# Analog to digital converter example.
# Will loop forever printing ADC channel 1 raw and mV values every second.
# NOTE the ADC can only read voltages in the range of ~900mV to 1800mV!

import time
import board
import busio
import adafruit_lis3dh
import digitalio
import microcontroller
import neopixel

#CONSTANT file Format
version = 2
delay = 0.095

time_start = 0
time_end = 0

# define custom colors
RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)
WHITE = (255, 255, 255)
OFF = (0, 0, 0)

pixels = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=0.2, auto_write=False)

button_stop = digitalio.DigitalInOut(board.D4)
button_stop.direction = digitalio.Direction.INPUT
button_stop.pull = digitalio.Pull.DOWN

button_start = digitalio.DigitalInOut(board.D5)
button_start.direction = digitalio.Direction.INPUT
button_start.pull = digitalio.Pull.DOWN

def save_data(file_open):
    pixels.fill(PURPLE)
    pixels.show();
    time_start = time.monotonic_ns()
    while True:
        x,y,z = (
            value for value in lis3dh.acceleration
        )
        file_open.write('{0:f} ; {1:f}; {2:f}\n'.format(x,y,z))
        #print('%0.3f ; %0.3f ; %0.3f' % (x,y,z))
        #file_open.flush()
        time.sleep(delay)
        if button_stop.value :
            time_end = time.monotonic_ns()
            pixels.fill(RED)
            pixels.show()
            file_open.flush()
            break;
    return time_end-time_start

def increase_color(color, wait, steps=32):
    r,g,b = color
    for i in range(steps):
        pixels.fill((int(r*i/steps), int(g*i/steps), int(b*i/steps)))
        time.sleep(wait)
        pixels.show()

i2c = busio.I2C(board.ACCELEROMETER_SCL, board.ACCELEROMETER_SDA)
lis3dh = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)
lis3dh.range = adafruit_lis3dh.RANGE_8_G

led = digitalio.DigitalInOut(board.D13)
led.switch_to_output()

try:
    name = 1
    while True:
        time_elapsed = 0
        if button_start.value:
            increase_color(BLUE, 0.1)
            with open("/tmp.txt", "w") as fp:
                time_elapsed = save_data(fp)
            with open("/accel-"+str(name)+".txt", "w") as f:
                with open("/tmp.txt", "r") as fp:
                    lines = fp.readlines()
                    f.write('version:{},delay:{}\n'.format(version, time_elapsed/(len(lines)*1000000000)))
                    for line in lines:
                        f.write(line)
                    f.flush()
            increase_color(GREEN,0.1)
            name += 1
        time.sleep(0.5)
except OSError as e:
    print(e)
    wait = 0.5
    if e.args[0] == 28:
        wait = 0.25
    while True:
        pixels.fill(RED)
        pixels.show()
        time.sleep(wait)
        pixels.fill(OFF)
        pixels.show()
        time.sleep(wait)
        
    

## Set range of accelerometer (can be RANGE_2_G, RANGE_4_G, RANGE_8_G or RANGE_16_G).
#a,b,c = 0, 0, 0
## Loop forever printing accelerometer values
#while True:
    ## Read accelerometer values (in m / s ^ 2).  Returns a 3-tuple of x, y,
    ## z axis values.  Divide them by 9.806 to convert to Gs.
        #a = max(x,a)
        #b = max(b,y)
        #c = max(c,z)
    ## Small delay to keep things responsive but give time for interrupt processing.
    #time.time.sleep(0.1)
