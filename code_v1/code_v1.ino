#include"PatternBase.h"
#include <arduino-timer.h>
Adafruit_NeoPixel pixels(10, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip(25, PIN_A3, NEO_GRB + NEO_KHZ800);

MyNeopixel pixel(pixels, 0, 10); 
MyNeopixel strips(strip, 0, 25); 

#define BUTTON_LEFT 4
#define BUTTON_RIGHT 5
#define BUTTON_SLIDE 7

color_t WHITE(0xff, 0xff, 0xff);
color_t PURPLE(0x80, 0x3f, 0xcb);
color_t BLUE(0x00, 0x3f, 0xcb);
color_t GREEN(0x59, 0xff, 0x59);
color_t YELLOW(0xff, 0xcc, 0x1f);
color_t RED(0xff, 0x00, 0x00);
color_t BLACK(0x00, 0x00, 0x00);
color_t ORANGE(0xFF, 0x7C, 0x00);


FillUnstablePattern rain(WHITE, strips, 4);
FadeUnstablePattern fadeunstable(BLACK, WHITE, strips, 10, 400);
FadePattern fade(BLACK, WHITE, strips, 100, true);
MultiFadePattern multi(strips, 1000);
RainbowPattern rainbow(strips);
ScannerPattern scanRedRuban(RED, strips);
FadePattern pattern(PURPLE, YELLOW, strips, 10);
FillRandomPattern fillWhiteRuban(YELLOW, strips, 3);
SimpleColorPattern colorRed(RED, strips);
SimpleColorPattern colorOrange(ORANGE, strips);
SimpleColorPattern colorWhite(WHITE, strips);
SimpleColorPattern colorGreen(GREEN, strips);


FillUnstablePattern rain1(WHITE, pixel, 4);
FadeUnstablePattern fadeunstable1(BLACK, WHITE, pixel, 10, 400);
FadePattern fade1(BLACK, WHITE, pixel, 100, true);
MultiFadePattern multi1(pixel, 1000);
RainbowPattern rainbow1(pixel);
ScannerPattern scanRedRuban1(RED, pixel);
FadePattern pattern1(PURPLE, YELLOW, pixel, 10);
FillRandomPattern fillWhiteRuban1(YELLOW, pixel, 3);
SimpleColorPattern colorRed1(RED, pixel);
SimpleColorPattern colorOrange1(ORANGE, pixel);
SimpleColorPattern colorWhite1(WHITE, pixel);
SimpleColorPattern colorGreen1(GREEN, pixel);


Timer<3, millis> timer; 

const uint8_t size_ruban = 11;

uint8_t index_ruban = 0;

context ruban_table[size_ruban] = {
 context(&multi, 20), 
 context(&rain, 20),
 context(&scanRedRuban , 200),
 context(&fadeunstable , 120),
 context(&fade , 10),
 context(&rainbow, 20),
 context(&fillWhiteRuban, 50),
 context(&colorRed, 100),
 context(&colorOrange, 100),
 context(&colorWhite, 100),
 context(&colorGreen, 100)
};

context module_table[size_ruban] = {
 context(&multi1, 20), 
 context(&rain1, 20),
 context(&scanRedRuban1 , 30),
 context(&fadeunstable1 , 120),
 context(&fade1 , 10),
 context(&rainbow1, 20),
 context(&fillWhiteRuban1, 50),
 context(&colorRed1, 100),
 context(&colorOrange1, 100),
 context(&colorWhite1, 100),
 context(&colorGreen1, 100)
 
};

bool module(void*)
{
  module_table[index_ruban].pattern->next();
  return true;
}

Timer<>::Task timer_ruban;
Timer<>::Task timer_module;

bool ruban(void*)
{
  ruban_table[index_ruban].pattern->next();
  return true;
}

bool checkbutton(void*)
{
  if(digitalRead(BUTTON_LEFT))
  {
    if(index_ruban < size_ruban-1)
    {
      index_ruban ++;
    } else
    {
      index_ruban = 0;
    }
    timer.cancel(timer_ruban);
    ruban_table[index_ruban].pattern->reinit();
    timer_ruban = timer.every(ruban_table[index_ruban].time_ms, ruban);
    timer.cancel(timer_module);
    module_table[index_ruban].pattern->reinit();
    timer_module = timer.every(module_table[index_ruban].time_ms, module);
  }
//
//    if(digitalRead(BUTTON_RIGHT))
//  {
//    if(index_ruban == 0)
//    {
//      index_ruban --;
//    } else
//    {
//      index_ruban = size_ruban -1;
//    }
//    timer.cancel(timer_ruban);
//    module_table[index_ruban].pattern->reinit();
//    timer_module = timer.every(module_table[index_ruban].time_ms, module);
//  }
  return true;
}

void setup() {
 pinMode(BUTTON_LEFT, INPUT_PULLDOWN);
  pinMode(BUTTON_RIGHT, INPUT_PULLDOWN);
  pinMode(BUTTON_SLIDE, INPUT_PULLUP);

  strip.setBrightness(20);
  pixels.setBrightness(20);
  // put your setup code here, to run once:
  timer_ruban = timer.every(ruban_table[index_ruban].time_ms, ruban);
  timer_module = timer.every(module_table[index_ruban].time_ms, module);
  timer.every(200, checkbutton);
  }

void loop() {
  // put your main code here, to run repeatedly:
  timer.tick();
}
